﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RunMeasurements : MonoBehaviour {

    public Text timeLabel = null;
    public Text distanceLabel = null;
    public Text pauseBtn = null;
    private int start = 0;
    private int current = 0;
    private int seconds = 0, minutes = 0, hours = 0;
    string h, m, s;
    private float distance;
    private bool paused = false;
    private int elapsed_time = 0;

    public GameObject addProgressPnl;
    public DataElementList list;

    // Use this for initialization
    void Start () {
        pauseBtn.text = "Pause";

    }
	
	// Update is called once per frame
	void Update () {
        //if the user paused current activity
        if(paused)
        {
            start = (int)Time.fixedTime - elapsed_time;
        }

        //if the application is not on pause and the user pressed start button
		if(start != 0 && !paused)
        {
            //current time
            current = (int) Time.fixedTime;

            //if the user pressed on pause button, we need to get rid of the time in wich he didn't run
            int elapsed = current - start;
            seconds = elapsed % 60;
            elapsed /= 60;
            minutes = elapsed % 60;
            elapsed /= 60;
            hours = elapsed / 60;

            //calculate and print actual time
            if(timeLabel != null)
            {
                h = ((hours > 9) ? hours.ToString() : ("0" + hours));
                m = ((minutes > 9) ? minutes.ToString() : ("0" + minutes));
                s = ((seconds > 9) ? seconds.ToString() : ("0" + seconds));

                    timeLabel.text = "Time: " +  h +":" + m + ":" + s;
            }
                
        }
        
	}

    public void OnStartPress()
    {
        start = (int) Time.fixedTime;
    }

    public void OnPausePress()
    {
        //if pause button was not pressed
        if(pauseBtn.text == "Pause")
        {
            paused = true;
            elapsed_time = (int)Time.fixedTime - start;
            pauseBtn.text = "Resume";
        }

        //if pause button was pressed and the text became "Resume"
        else if(pauseBtn.text == "Resume")
        {
            paused = false;
            pauseBtn.text = "Pause";
        }
    }

    public void OnStopPress()
    {
        //end of activity: time become 0 again
        start = 0;
        timeLabel.text = "Time: 00:00:00";

        //check if empty
        string time = h + ":" + m + ":" + s, distance = "5km";
        Debug.Log(time);
        list.AddProgress(time, distance);
    }


}
