﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPS : MonoBehaviour
{

    public static GPS Instance = null;

    public GPS()
    {
        if(GPS.Instance == null)
        {
            GPS.Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public float latitude;
    public float longitude;

	// Use this for initialization
	void Start ()
    {
        DontDestroyOnLoad(gameObject);
        StartCoroutine(StartLocationService());
	}

    private IEnumerator StartLocationService()
    {
        if(!Input.location.isEnabledByUser)
        {
            yield break;
        }

        Input.location.Start();
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
            if (maxWait <= 0)
            {
                Debug.Log("Timed out");
                yield break;
            }
        }

        if (Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.Log("Unable to determine device location");
            yield break;
        }

        latitude = Input.location.lastData.latitude;
        longitude = Input.location.lastData.longitude;

        yield break;
    }
}
