﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FAKButtonBehaviour : MonoBehaviour {

    private string objName;
    private int pnlIndex = -1;

    public Text TitleTxt = null;

    // Use this for initialization
    void Start()
    {
        objName = gameObject.name;

        //iterrate through the array
        for (int i = 0; i < transform.parent.transform.childCount; i++)
        {

            //search through the element list and display the correct panel
            if (transform.parent.GetChild(i).gameObject.name == objName)
            {
                pnlIndex = i;
                break;
            }
        }
    }

    public void FAKOnPress()
    {
        //if there are no panels, return
        if (pnlIndex == -1)
            return;

        FAKPanelSelector selector = FindObjectOfType<FAKPanelSelector>();
        
        //activate a specific panel
        selector.ActivatePanel(pnlIndex);

        if (TitleTxt != null)
            TitleTxt.text = this.gameObject.GetComponentInChildren<Text>().text;
    }
}
