﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonBehaviour : MonoBehaviour {

    private string objName;
    private int pnlIndex = -1;

    public Text TitleTxt = null;

	// Use this for initialization
	void Start () {
        objName = gameObject.name;
        for (int i = 0; i < transform.parent.transform.childCount; i++)
        {
            if(transform.parent.GetChild(i).gameObject.name == objName)
            {
                pnlIndex = i;
                break;
            }
        }
    }
	
    public void OnPress()
    {
        if (pnlIndex == -1)
            return;

        PanelSelector selector = FindObjectOfType<PanelSelector>();
        selector.ActivatePanel(pnlIndex);

        if(TitleTxt != null)
            TitleTxt.text = this.gameObject.GetComponentInChildren<Text>().text;
    }
}
