﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataElementList : MonoBehaviour {

    public GameObject dataElementObject = null;

    [SerializeField]
    private List<GameObject> dataElementList = new List<GameObject>();

	// Use this for initialization
	void Start ()
    {
        if (dataElementObject == null) Destroy(this);
	}

    public void InitList(List<DataElementBehaviour> list)
    {
        //iterrate through the array
        foreach(DataElementBehaviour element in list)
        {
            //for each element in list, create a new object
            GameObject obj = new GameObject();
        }
    }

    //add a new element
    public void AddProgress(string duration, string distance)
    {
        GameObject dataElement = Transform.Instantiate(dataElementObject, transform);
        DataElementBehaviour element = dataElement.GetComponent<DataElementBehaviour>();
        element.SetDistance(duration);
        element.SetDuration(distance);
        element.UpdateVal();
    }
}
