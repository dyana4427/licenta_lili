﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginRegisterSwitchBehaviour : MonoBehaviour {

    public GameObject registerPage = null, loginPage = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SwitchToRegister()
    {
        registerPage.SetActive(true);
        loginPage.SetActive(false);
    }

    public void SwitchToLogin()
    {
        registerPage.SetActive(false);
        loginPage.SetActive(true);
    }

}
