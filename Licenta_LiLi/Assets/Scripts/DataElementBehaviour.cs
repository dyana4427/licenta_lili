﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(LayoutElement))]
public class DataElementBehaviour : MonoBehaviour {

    [SerializeField]
    Text distance = null;
    [SerializeField]
    Text duration = null;
    [SerializeField]
    Image map = null;
    [SerializeField]
    int mapSizeMultiplier = 5;

    string dist = "", dur = "";
    

    LayoutElement layout;
    bool expanded = false;

	// Use this for initialization
	void Start () {
        layout = gameObject.GetComponent<LayoutElement>();
        layout.preferredWidth = transform.parent.GetComponent<RectTransform>().rect.width - 50;
        layout.preferredHeight = transform.parent.GetComponent<RectTransform>().rect.height / 8;
        if (layout.preferredHeight < layout.minHeight) layout.preferredHeight = layout.minHeight;
        map.gameObject.SetActive(false);
        //
        //distance = transform.Find("DistanceText").gameObject.GetComponent<Text>();
        //duration = transform.Find("DurationText").gameObject.GetComponent<Text>();
        //map = transform.Find("Map").gameObject.GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPress()
    {
        if (expanded)
        {
            layout.preferredHeight /= mapSizeMultiplier;
            expanded = false;
            map.gameObject.SetActive(false);
        }
        else
        {
            layout.preferredHeight *= mapSizeMultiplier;
            expanded = true;
            map.gameObject.SetActive(true);
        }
    }

    public void SetDistance(string dist)
    {
        this.dist = dist;
    }

    public void SetDuration(string dur)
    {
        this.dur = dur;
    }

    public void SetMap(Image _map)
    {
        //map.gameObject.GetComponent<Image>().mainTexture. = _map.mainTexture;
    }

    public string GetDistance()
    {
        return this.dist;
    }

    public string GetDuration()
    {
        return this.dur;
    }

    public Image GetMap()
    {
        return map;
    }

    public void UpdateVal()
    {
        duration.text = "Distance: " + this.dur;
        distance.text = "Time: " + this.dist;
    }
}
