﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FAKPanelSelector : MonoBehaviour {

    private GameObject content = null;
    public Text TitleTxt = null;

    private List<GameObject> fakPanelList = new List<GameObject>();

    // Use this for initialization
    void Start()
    {
        content = this.gameObject;

        if (content != null)
        {
            //iterrate throught the panels list
            for (int i = 0; i < content.transform.childCount; i++)
            {
                fakPanelList.Add(content.transform.GetChild(i).gameObject);
            }

            //iterrate throught the panels list
            foreach (GameObject pnl in fakPanelList)
            {
                pnl.SetActive(false);
            }
            fakPanelList[0].SetActive(true);

            //the first panel display will be the first element from the list
            if (TitleTxt != null)
                TitleTxt.text = "Burn";
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    //activate a specific panel
    public void ActivatePanel(int idx)
    {
        //iterrate throught the panels list
        foreach (GameObject pnl in fakPanelList)
        {
            //make each panel inactive
            pnl.SetActive(false);
        }

        //make active only the panel wich correspond to the selected button
        fakPanelList[idx].SetActive(true);
    }
}
