﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateNewGoalBehaviour : MonoBehaviour {

    public GameObject addGoalPnl;

    public Text goalNameNOK, initialValNOK, targetValNOK;

    public InputField goalName, initialVal, targetVal;
    public GoalsElementList list;

    //open the panel for adding a new goal
    public void ActivatePanel()
    {
        addGoalPnl.SetActive(true);
    }

    public void PanelOK()
    {
        //check if empty
        string goalN = goalName.text, goalIV = initialVal.text, goalT = targetVal.text;

        //if none of the input fields are empty, a new goal will be added to the list
        if (goalN != "" && goalIV != "" && goalT != "")
        {
            list.AddElement(goalN, goalIV, goalT);

            //after the element is added into the list, panel will disapear
            addGoalPnl.SetActive(false);
        }   

        //if the user left one or multiple input fields empty
        else
        {
            if (goalN == "")
                goalNameNOK.text = "Please specify at least the name for your goal!";
            if (goalIV == "")
                initialValNOK.text = "Please specify the initial value of your goal!";
            if (goalT == "")
                targetValNOK.text = "Please specify the target of your goal!";
        }
    }

    public void PanelCancel()
    {
        goalName.text = initialVal.text = targetVal.text = "";
        addGoalPnl.SetActive(false);
    }
}
