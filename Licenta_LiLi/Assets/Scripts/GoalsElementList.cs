﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalsElementList : MonoBehaviour {

    public GameObject goalsElementObject = null;

    [SerializeField]
    private List<GameObject> goalsElementList = new List<GameObject>();

    // Use this for initialization
    void Start () {
        if (goalsElementObject == null) Destroy(this);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void InitList(List<GoalsElementBehaviour> list)
    {
        foreach (GoalsElementBehaviour element in list)
        {
            GameObject obj = new GameObject();
            //obj.AddComponent
        }
    }

    //add a new goal
    public void AddElement(string goalName, string goalInitVal, string goalTarget)
    {
        GameObject goalsElement = Transform.Instantiate(goalsElementObject, transform);
        GoalsElementBehaviour element = goalsElement.GetComponent<GoalsElementBehaviour>();
        element.SetGoalName(goalName);
        element.SetInitialValue(goalInitVal);
        element.SetGoalTarget(goalTarget);
        element.UpdateGoals();
    }
}
