﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelSelector : MonoBehaviour {

    private GameObject content = null;
    public Text TitleTxt = null;

    private List<GameObject> panelList = new List<GameObject>();

	// Use this for initialization
	void Start () {
        content = this.gameObject;

		if(content != null)
        {
            for(int i = 0; i < content.transform.childCount; i++)
            {
                panelList.Add(content.transform.GetChild(i).gameObject);
            }
            foreach(GameObject pnl in panelList)
            {
                pnl.SetActive(false);
            }
            panelList[0].SetActive(true);

            if (TitleTxt != null)
                TitleTxt.text = "Progress";
        }

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ActivatePanel(int idx)
    {
        foreach (GameObject pnl in panelList)
        {
            pnl.SetActive(false);
        }
        panelList[idx].SetActive(true);
    }
}
