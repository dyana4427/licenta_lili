﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(LayoutElement))]
public class GoalsElementBehaviour : MonoBehaviour {

    [SerializeField]
    Text goalName = null;
    [SerializeField]
    Text initialValue = null;
    [SerializeField]
    Text goalTarget = null;
    [SerializeField]
    int goalSizeMultiplier = 5;

    string goalNameT = "", initialValueT = "", goalT = "";

    LayoutElement layout;
    bool expanded = false;

    // Use this for initialization
    void Start () {
        layout = gameObject.GetComponent<LayoutElement>();
        layout.preferredWidth = transform.parent.GetComponent<RectTransform>().rect.width - 50;
        layout.preferredHeight = transform.parent.GetComponent<RectTransform>().rect.height / 8;
        if (layout.preferredHeight < layout.minHeight) layout.preferredHeight = layout.minHeight;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPress()
    {
        if (expanded)
        {
            layout.preferredHeight /= goalSizeMultiplier;
            expanded = false;
            goalTarget.gameObject.SetActive(false);
        }
        else
        {
            layout.preferredHeight *= goalSizeMultiplier;
            expanded = true;
            goalTarget.gameObject.SetActive(true);
        }
    }

    public void SetGoalName(string w)
    {
        this.goalNameT = w;
    }

    public void SetInitialValue(string d)
    {
        this.initialValueT = d;
    }

    public void SetGoalTarget(string g)
    {
        this.goalT = g;
    }

    public string GetGoalName()
    {
        return this.goalNameT;
    }

    public string GetInitialValue()
    {
        return this.initialValueT;
    }

    public string GetGoalTarget()
    {
        return this.goalT;
    }

    public void UpdateGoals()
    {
        goalName.text = "Goal name, description: " + this.goalNameT;
        initialValue.text = "Initial value: " + this.initialValueT;
        goalTarget.text = "Target value: " + this.goalT;
    }
}
