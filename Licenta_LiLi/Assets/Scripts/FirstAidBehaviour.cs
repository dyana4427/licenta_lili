﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstAidBehaviour : MonoBehaviour {

    public GameObject firstAidPnl;
    

    // Use this for initialization
    void Start () {
        firstAidPnl.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void FirstAidPanelOpen()
    {
        if (firstAidPnl.activeInHierarchy == false)
            firstAidPnl.SetActive(true);
        else if (firstAidPnl.activeInHierarchy == true)
            firstAidPnl.SetActive(false);
    }
}
