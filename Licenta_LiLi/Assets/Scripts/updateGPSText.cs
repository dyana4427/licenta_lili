﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class updateGPSText : MonoBehaviour
{

    public Text coordinates = null;

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(coordinates != null)
            coordinates.text = "Lat: " + GPS.Instance.latitude.ToString() + "Lon:" + GPS.Instance.longitude.ToString();
    }
}
