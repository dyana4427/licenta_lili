﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.IO;

public class LoginBtnBehaviour : MonoBehaviour {

    public GameObject mainScreen = null;
    public InputField username, password, email;

    public void Login()
    {
        mainScreen.SetActive(true);
    }

    public void WriteString()
    {
        string path = "Assets/Resources/test.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(username.text);
        writer.WriteLine(password.text);
        writer.WriteLine(email.text);
        writer.WriteLine("----------------------------------");
        writer.Close();

    }
}
