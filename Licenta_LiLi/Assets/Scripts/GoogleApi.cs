﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
[RequireComponent(typeof(RectTransform))]
public class GoogleApi : MonoBehaviour
{

    private RawImage img;
    private Rect rect;

    string url;

    LocationInfo li;

    public int zoom = 13;
    public int mapWidth = 260;
    public int mapHeight = 260;

    public int scale;
    public Text debug;

    private Vector2 previousPos, currentPos;
    private Vector2 movementVector;

    private Vector2 currentCoords;

    float mapSlideSpeed = 0.0085f;

    IEnumerator Map()
    {
        while(true)
        {
            url = "https://maps.googleapis.com/maps/api/staticmap?center=" + currentCoords.x.ToString() + "," + currentCoords.y.ToString() +
        "&zoom=" + zoom + "&size=" + mapWidth + "x" + mapHeight + "&scale=" + scale
        + "&maptype=roadmap" +
        "&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284&key=AIzaSyDs5-hb1eQy-jjJYdekK-Vg1cDD3pvhDzM";
            WWW www = new WWW(url);
            Debug.Log(url);
            yield return new WaitUntil(() => www.isDone);
            Debug.Log("Texture set");
            img.texture = www.texture;
            img.SetNativeSize();
        }
    }



	void Start ()
    {
        img = this.gameObject.GetComponent<RawImage>();
        rect = gameObject.GetComponent<RectTransform>().rect;
        

        previousPos = currentPos = new Vector2(rect.width/2, rect.height/2);
        movementVector = new Vector2();
        currentCoords = new Vector2(45.75671f, 21.24661f);
        //currentCoords = new Vector2(GPS.Instance.latitude, GPS.Instance.longitude);

        StartCoroutine(Map());
    }
	
	// Update is called once per frame
	void Update ()
    {

        float w = transform.parent.GetComponent<RectTransform>().rect.width;
        float h = transform.parent.GetComponent<RectTransform>().rect.height;
        float x = transform.parent.GetComponent<RectTransform>().rect.x;
        float y = transform.parent.GetComponent<RectTransform>().rect.y;

        w = h = (w < h) ? (w - w * 5 / 100) : (h - h * 5 / 100);
        mapWidth = mapHeight = (int)w;


        rect.Set(x, y, w, h);

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            // Handle finger movements based on TouchPhase
            switch (touch.phase)
            {
                //When a touch has first been detected, change the message and record the starting position
                case TouchPhase.Began:
                    // Record initial touch position.
                    previousPos = currentPos = touch.position;
                    break;

                //Determine if the touch is a moving touch
                case TouchPhase.Moved:
                    // Determine direction by comparing the current touch position with the initial one
                    currentPos = touch.position;
                    Vector2 movement = (-(currentPos - previousPos)).normalized;
                    movement.y = -movement.y;
                    debug.text = "Movement vector: " + movement;
                    currentCoords += (movement / zoom)* mapSlideSpeed;
                    break;

                case TouchPhase.Ended:
                    // Report that the touch has ended when it ends
                    break;
            }
        }
    }
}
