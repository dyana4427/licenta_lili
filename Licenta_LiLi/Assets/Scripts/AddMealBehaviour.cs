﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddMealBehaviour : MonoBehaviour
{
    //meal(s) name introduced by the user
    public InputField meal;

    //meal will be saved in one of these variable depending on the type of the meal
    public Text breakfastTxt, lunchTxt, dinnerTxt, snacksTxt;

    //meal type
    public Dropdown type;

    public void OnPress()
    {
        //if the meal is for breakfast
        if(type.options[type.value].text == "Breakfast")
        {
            //save the meal in the breakfastTxt variable
            breakfastTxt.text = meal.text;
        }

        //if the meal is for lunch
        if (type.options[type.value].text == "Lunch")
        {
            //save the meal in the lunchTxt variable
            lunchTxt.text = meal.text;
        }

        //if the meal is for dinner
        if (type.options[type.value].text == "Dinner")
        {
            //save the meal in the dinnerTxt variable
            dinnerTxt.text = meal.text;
        }

        //if the meal is for snack(s)
        if (type.options[type.value].text == "Snacks")
        {
            //save the meal in the snacksTxt variable
            snacksTxt.text = meal.text;
        }
    }
}
